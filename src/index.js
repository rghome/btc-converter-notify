import axios from 'axios'

import notifier from 'node-notifier'

(async () => {
  const have = {
    usd: 350,
    btc: 0.043
  }
  const meta = {
    appIcon: require('path').join(__dirname, '../opengraph.png'),
    actions: 'Profit',
    title: 'BTC Rate Helper',
    fixed: 3,
    timeout: 50
  }

  let nc = new notifier.NotificationCenter()

  const responseHandler = (err, response, metadata) => {
    if (metadata.activationValue === 'Profit') {
      nc.notify({
        appIcon: meta.appIcon,
        title: meta.title,
        timeout: meta.timeout,
        message: `${meta.actions}: ${profit}`,
        wait: true,
        closeLabel: 'Close',
      });
    }
  }

  const getProviderData = async () => {
    const response = await axios.get('https://api.coindesk.com/v1/bpi/currentprice.json')

    let data = response.data

    let rate = parseFloat(data.bpi.USD.rate_float).toFixed(meta.fixed)
    let currentPrice = parseFloat(rate * have.btc).toFixed(meta.fixed)
    let profit =  parseFloat(currentPrice-have.usd).toFixed(meta.fixed)

    return [rate, currentPrice, profit]
  }

  let [rate, currentPrice, profit] = await getProviderData()

  const message = [
    `BTC Price: ${rate}`,
    `Current Price: ${currentPrice}`,
    // `Profit: ${profit}`
  ]

  nc.notify({
    icon: meta.appIcon,
    title: meta.title,
    timeout: meta.timeout,
    message: message.join("\r"),
    sound: 'Bottle',
    wait: true,
    closeLabel: 'Close',
    actions: meta.actions
  }, responseHandler);


})()